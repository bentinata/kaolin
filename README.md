```
      _ _
     / / )
    / / /
   ( ( /     _
   (\ \ \   / )
   (\\ \ \_/ /
    \       /
     \    _/
     /   /
    /   /
   /   /
```

Kaolin, the Commit Gripper

# What?

Kaolin is a simple http daemon
that will pull latest changes
on specified git directory.

# Why?

I need http server that just do this exact thing.
While also learn how to implement http server myself.

# How?

Clone, submodule, make.

```bash
git clone https://gitlab.com/bentinata/kaolin
cd kaolin
git submodule update
make
./kaolin --port 7920 &
```

Or just change port to any port you wish.

Yeah, I know, it lacks documentation.
I accept help for that kind of things. :peach:

# License

[MIT](https://opensource.org/licenses/MIT)
