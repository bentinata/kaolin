#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <argp.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "lib/parson/parson.c"

const char* argp_version = "kaolin 1.0.0";
const char* argp_bug = "Ben N <bentinata@gmail.com>";

static char doc[] = "Simple http server to listen for hook.";

static struct argp_option options[] = {
  {"port",    'p', "port",    0, "Specify different port than 80"},
  {"config",  'c', "config",  0, "Path to kaolin.json config files"},
  {0}
};

struct args {
  int port;
  char* config;
};

static error_t parser(int key, char* arg, struct argp_state* state) {
  struct args* args = state->input;

  switch (key) {
    case 'p':
      args->port = atoi(arg);
      break;
    case 'c':
      args->config = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

static struct argp argp = {options, parser, arg_doc, doc};

void error(char *message) {
  perror(message);
  exit(1);
}

int main(int argc, char** argv) {
  struct args args = {80, "kaolin.json"};
  argp_parse(&argp, argc, argv, 0, 0, &args);

  int serv_sock;
  int clnt_sock;

  int i = 1;
  int content_length;

  char req_buff[1024];
  char res_buff[64];
  char* name;
  char* path;

  struct sockaddr_in serv_addr;
  struct sockaddr_in clnt_addr;

  socklen_t clnt_len;

  JSON_Object* json;
  JSON_Object* repos;

  repos = json_object(json_parse_file(args.config));

  serv_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (serv_sock == -1)
    error("error opening socket");

  if (setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(int)) < 0)
    error("setsockopt failed");

  // Allocate needed memory for server socket.
  memset(&serv_addr, 0, sizeof(serv_addr));

  // Set server socket properties.
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(args.port);

  if (bind(serv_sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    error("error binding");

  listen(serv_sock, 5);
  clnt_len = sizeof(clnt_addr);

  printf("Kaolin running on port %d\n", args.port);
  // Keep listening to client request.
  while (1) {
    clnt_sock = accept(serv_sock, (struct sockaddr *) &clnt_addr, &clnt_len);
    if (clnt_sock == -1)
      error("error accepting client");

    // Ready up memory.
    memset(&req_buff, 0, 1024);
    if (read(clnt_sock, req_buff, sizeof(req_buff)) == -1)
      error("error reading socket");

    json = json_object(json_parse_string(strrchr(req_buff, '\n')+1));

    name = json_object_dotget_string(json, "project.name");
    if (name != 0) {
      for (char *p = name; *p; ++p) *p = tolower(*p);

      path = json_object_get_string(repos, name);
      if (path != 0) {
        execl(path, "git pull");
      }
    }

    //printf("len: %d, %s\n", content_length, req_cache);

    sprintf(res_buff, "\r\n");
    send(clnt_sock, res_buff, strlen(res_buff), 0);

    close(clnt_sock);

  }

  close(serv_sock);

  return 0;

}
